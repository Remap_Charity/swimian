#include <Wire.h>
#include <LSM303.h>
#include "vector.c"

#include <Arduino.h>
// last update 2/6/2018
/*
   Tilt compensated compass by Pololu engineers, with mods by SJR
   This program assumes that an LSM303D carrier is oriented with X pointing
   North, Y pointing East, and Z pointing down (toward the ground).
   The code compensates for tilts of up to 90 degrees away from horizontal.
   Vector p, the "pointing vector" should be defined as pointing forward, parallel 
   to the ground, with coordinates {X, Y, Z} in the sensor frame of reference.
*/
vector p = {1, 0, 0};

LSM303 compass;

char report[80];

// magnetometer offsets, uninitialized
// can be replaced by the output of the calibration step, once the sensor is in place

vector m_max = { -697, -368, -768};
vector m_min = {484, 864, 438};

// Function Prototypes
void read_data(vector *a, vector *m);
int get_heading(const vector *a, const vector *m);
void debounceInterrrupt();
void setHeading();


int sounder = 8;
volatile long debounce_time = 500;
volatile unsigned long last_micros;
const int SetHeadingButton = 2;
volatile int newHeading;
volatile int currentHeading;

void setup()
{
  vector a, m;
  pinMode(SetHeadingButton, INPUT_PULLUP); // BUTTON
  attachInterrupt(digitalPinToInterrupt(SetHeadingButton), debounceInterrrupt, RISING);
  tone(sounder, 1000, 500);
  Serial.begin(9600);
  Wire.begin();
  compass.init();
  compass.enableDefault();
  unsigned long start = millis();
  Serial.println("Calibrating...");

  // calibration,
  // rotate sensor through all possible orientations, accumulating max and min
  // loop until nothing changes for 5 seconds

  while (millis() - start < 5000UL) {
    read_data(&a, &m);

    if (m.x < m_min.x) {
      m_min.x = m.x;
      start = millis();
    }
    if (m.x > m_max.x) {
      m_max.x = m.x;
      start = millis();
    }
    if (m.y < m_min.y) {
      m_min.y = m.y;
      start = millis();
    }
    if (m.y > m_max.y) {
      m_max.y = m.y;
      start = millis();
    }
    if (m.z < m_min.z) {
      m_min.z = m.z;
      start = millis();
    }
    if (m.z > m_max.z) {
      m_max.z = m.z;
      start = millis();
    }
  }
  Serial.println("magnetometer offset constants X, Y, Z");
  Serial.print(m_min.x);
  Serial.print(", ");
  Serial.print(m_max.x);
  Serial.print(", ");
  Serial.print(m_min.y);
  Serial.print(", ");
  Serial.print(m_max.y);
  Serial.print(", ");
  Serial.print(m_min.z);
  Serial.print(", ");
  Serial.println(m_max.z);
}
// Returns a set of acceleration and magnetic readings from the cmp01c.
void read_data(vector *a, vector *m)
{
  compass.read();
  a->x = compass.a.x;
  a->y = compass.a.y;
  a->z = compass.a.z;
  m->x = compass.m.x;
  m->y = compass.m.y;
  m->z = compass.m.z;
}

// Returns a heading (in degrees) given an acceleration vector a due to gravity, a magnetic vector m, and a facing vector p (global).
int get_heading(const vector *a, const vector *m)
{
  vector E;
  vector N;

  // D X M = E, cross acceleration vector Down with M (magnetic north + inclination) to produce "East"
  vector_cross(a, m, &E);
  vector_normalize(&E);

  // E X D = N, cross "East" with "Down" to produce "North" (parallel to the ground)
  vector_cross(&E, a, &N);
  vector_normalize(&N);

  // compute heading, get Y and X components of heading from E dot p and N dot p
  int heading = round(atan2(vector_dot(&E, &p), vector_dot(&N, &p)) * 180 / M_PI);
  if (heading < 0)
    heading += 360;
  heading = 360 - heading;
  currentHeading = heading;
  return currentHeading;
}

void debounceInterrrupt() {
    if((long)(micros() - last_micros) >= debounce_time * 1000) {
        setHeading();
        last_micros = micros();
    }
}

void setHeading()
{
    newHeading = currentHeading;
}

void loop()
{
  vector a, m;
  read_data(&a, &m);
  // shift and scale magnetometer data
  m.x = (m.x - m_min.x) / (m_max.x - m_min.x) * 2 - 1.0;
  m.y = (m.y - m_min.y) / (m_max.y - m_min.y) * 2 - 1.0;
  m.z = (m.z - m_min.z) / (m_max.z - m_min.z) * 2 - 1.0;
  // same for accelerometer. Should also consider offsets and scale here...
  a.x /= 16384.;
  a.y /= 16384.;
  a.z /= 16384.;

  /* for the curious...
    Serial.print(m.x);
    Serial.print(", ");
    Serial.print(m.y);
    Serial.print(", ");
    Serial.print(m.z);
    Serial.print(", ");
    Serial.print(a.x);
    Serial.print(", ");
    Serial.print(a.y);
    Serial.print(", ");
    Serial.println(a.z);
  */
  int deviation = 10;
  

  int error = currentHeading - newHeading;

    if (error >  180) error -= 360;
    if (error < -180) error += 360;

  if(error  >  deviation)
    {
        tone(sounder, 1000, 50);
    }
  else if(error  <  (0 - deviation))
  {
      tone(sounder, 3000, 50 );
  }
  else
  {
      tone(sounder, 5000, 5 );
  }

    Serial.print("Heading: ");
    Serial.print(get_heading(&a, &m));
    Serial.print("  New Heading: ");
    Serial.print(newHeading);
    Serial.print("  Error: ");
    Serial.println(error);
    delay(500);
}